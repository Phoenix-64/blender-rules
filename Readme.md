# Blender Rules!
A friendly reddit bot, always happy to give post advice on r/Blender whenever he can. He's got very basic regex brains, but a kind heart.

[You can find the bot in action here!](https://www.reddit.com/user/blender-rules-bot)

Feedback is always welcome.
