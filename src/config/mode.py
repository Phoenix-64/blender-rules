from config.env import MODE

PRODUCTION_MODE = "PRODUCTION"
DRY_RUN_MODE = "DRY_RUN"

ALL_MODES = {PRODUCTION_MODE, DRY_RUN_MODE}

assert MODE in ALL_MODES


def is_production():
    return MODE == PRODUCTION_MODE


def is_dry_run():
    return MODE == DRY_RUN_MODE
