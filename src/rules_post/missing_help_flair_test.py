from rules_post.missing_help_flair import find_help_keywords

expected_help_keywords = {
    "i have a problem with the displacement it doesn't show up unlike blender guru": ["i have a problem",
                                                                                      "doesn't show up"],
    "Is there a way to get consistency with the scale when you transfer the model in between the softwares and also be able to preserve the subdivisions in zbrush?": [
        "Is there a way"],
    "I'm UV mapping a model, and I got to the wings. With everything else, I'd just select the part, unwrap it, and reposition it. The wings aren't working though. Selecting them doesnt bring them in the mapping tab, and unwraping them doesn't work either. Anyone know how to fix it?": [
        "aren't working", "doesn't work", "how to fix it?"],
    "i've spareted the skirt from the rest of the body to fix it's bones because they stuck to the hair, but I can't seem to find the skirt bones? when I go to pose mode theres only the rest of the body": [
        "I can't"],
    "Hi, I'm not sure if this has been asked or not but I couldn't find it I tried googling all day yesterday.": [
        "not sure if this has been asked", "I couldn't"],
    "I found out about a falloff node that helped me somewhat but it also doesn't work really well with more than 2 colors.": [
        "doesn't work"],
    "Can any of you please help. I'll really appreciate it. I'm fairly new to blender and nodes so a little explanation will also help.": [
        "please help"],
    "ISSUE: Tried out rigging and this issue appeared. Why isn't the armature aligned with the weapon barrel when rotated in Pose mode? The weapon barrel is at 45-d roughly": [
        "ISSUE", "issue appeared"],
    "is there a way to make the way objects react to light in eevee better? (comparison)": ["is there a way"],
    "How do you import a video animation from Blender into Unity?": ["How do you"],
    "I have this animation I made that is a 120 keyframes long, but I have no clue as to how I could possibly import it into unity.": [
        "I have no clue"],
    "Just started with blender, I'm a mess, but still curious. I can't see vertices on my mesh for some reason. Does anyone know why?": [
        "I can't", "Does anyone know"],
    "How do I get this menu to appear?": ["How do I"],
    "Bagapie Issues": ["Bagapie Issues"],
    "Hey, I'm having issues with Bagapie Addon. I've created a wall with a door way but when I tried rendering it. It won't show up (Shown in these screenshots) Please help me out.": [
        "I'm having issues", "won't show up", "Please help"],
    "Anyone run into a similar issue installing the new substance 3d add-on? It's an odd error and I can't find anything about it on Google but I bet it has something to do with conflicting files or installs or something.": [
        "error", "I can't"],
    "Why aren't the BlenderKit textures not exporting and how do I export just the textures manually?": ["how do I"],
    "Is it possible to make these shapes in Geometry Nodes instead of Shader Editor?": ["Is it possible to"],
    "I need help? I just got into 3d printing lately and honestly i don't know what is going on.": ["need help",
                                                                                                    "i don't know",
                                                                                                    "what is going on"],
    "How do I get displacement maps from a real-life surface? I got the stone statue on the picture from mixer, so I thought if I can scan something of my own. I know meshroom, but that creates 3d assets, what I need is displacement maps and not 3d meshes. Can anyone help?": [
        "How do I", "Can anyone help"],
    "I'm in a bit of pinch, I have this problem whenever i put an image as a texture on the floor... it seems there wont be any shadow cast on it. im really bad with blender so anyone who can help me would be great. Thank you in advance": [
        "I have this problem", "help me"],
    "I cant get the spikes to align with the reference images": ["I cant"],
    "The spikes align in the back view but the side view is wrong i don't know what i did.": ["i don't know"],
    "HELP. How can I smooth that texture from the sides (has texture in the oyher side as well)? I've applied subdivision surface and shade smooth already. Thanks": [
        "HELP", "How can I"],
    "My viewport is fairly bright but the render comes out very dark. Any help is appreciated.": [
        "help is appreciated"],
    "Does someone know why 'copy rotation' is behaving so weirdly? Probably I should have shown the IK tab as well.": [
        "Does someone know"],
    "Please advise on the most efficient way to import mu AC18 model into Blender to create fast animations. Thanks": [
        "Please advise"],
    "I was proud of this barrel..If I figure out how to unwrap it I may do some actual texture painting.": [],
}


def test_find_help_keywords():
    for title, expected_keywords in expected_help_keywords.items():
        assert sorted(find_help_keywords(title)) == sorted(expected_keywords)
