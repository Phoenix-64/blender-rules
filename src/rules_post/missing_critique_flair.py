import re

from bot.response import Response
from config import blender_flairs as flairs
from language_util.bot_language import join_keywords
from language_util.common_regex import somebody_keywords, could_keywords, please_keywords

criticism_keywords = "(criticisms?|suggestions|tips|thoughts|advice|input|ideas)"

CRITIQUE_KEYWORDS = re.compile(
    f"\\b(critiques?|feedback|constructive advice)\\b|"

    f"\\b(needs|((would|will) )?(like|want|appreciate)) (\\w+ ){{0,2}}{criticism_keywords}\\b|"
    f"\\b{could_keywords} (i (have|receive|get|hear)|{somebody_keywords} give) (\\w+ ){{0,4}}{criticism_keywords}\\b|"
    f"\\b{criticism_keywords}( \\w+){{0,4}} ?({please_keywords}|[^\\.!]*\\?)|"

    f"\\b(how|what) (i )?(can|could|would) (i )?(improve)\\b|"
    f"\\b(what do (\\w+ ){{0,2}}think)\\b|"

    f"\\b(what'?s|what is|anything|something) missing [^\\.!]+(\\?|:)",
    flags=re.IGNORECASE
)


def find_critique_keywords(text):
    return [
        match.group()
        for match in CRITIQUE_KEYWORDS.finditer(text)
    ]


def construct_advice(keywords):
    joined_keywords = join_keywords(keywords)

    return f"""
I noticed {joined_keywords} in your post. Are you asking for feedback on your work? If so, you can use the "Need Feedback" flair. If you are asking for help on a specific task, you can add the "Help!" flair.

This will make it easier for others to notice you're asking for tips!
"""


# flairs that the user might incorrectly add when asking for feedback
WRONG_FLAIRS = {
    flairs.I_MADE_THIS,
}


def get_missing_critique_flair_response(submission):
    flair = submission.link_flair_template_id if hasattr(submission, "link_flair_template_id") else None

    if flair not in WRONG_FLAIRS:
        return Response()

    keywords = [
        *find_critique_keywords(submission.title),
        *find_critique_keywords(submission.selftext),
    ]

    if len(keywords) == 0:
        return Response()

    return Response(
        reply=construct_advice(keywords)
    )
