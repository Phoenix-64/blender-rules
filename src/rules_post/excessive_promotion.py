import re

from bot.response import Response
from config.links import MAKING_A_GOOD_POST
from language_util.bot_language import construct_report, join_keywords

social = "(yt|youtube|channel|handle|twitch|insta|instagram|ig|artstation|socials)"

PROMOTION_KEYWORDS = re.compile(
    f"\\b{social}( \\w+)?(:| @[^\\s]+| account\\b)|"
    # need to exclude youtube from the below, as valid posts can end in "youtube tutorial"
    f"\\b(channel|twitch|insta|instagram) [^\\s]+$|"
    f"\\b(my) (\\w+ ){{0,5}}{social}\\b|"
    "\\b(subscribe|follow me)\\b|"
    "\\b(please) (share|like|follow)\\b|"
    "\\b(\\d+).{0,10}(likes|followers|subscribers)\\b|"
    "(\\b|^)(https?://.+\..+)(\\b|$)",
    flags=re.IGNORECASE
)


def find_promo_keywords(title):
    return {
        violation.group()
        for violation in PROMOTION_KEYWORDS.finditer(title)
    }


def construct_advice(keywords):
    mistakes = join_keywords(keywords)

    return f"""
I noticed {mistakes} in the title. Please do not put any references to social media or any links in the title – these things belong in the comments or post body for those interested. Instead, the title should be a concise description of what you are posting.

Could you please fix this by re-posting with a more informative title? The primary purpose of this subreddit is to let users share their work and learn. Excessive promotion may be removed at the moderators' discretion.

Also, please read [this quick guide]({MAKING_A_GOOD_POST}) before posting. Thanks!
"""


def get_excessive_promotion_response(submission):
    keywords = find_promo_keywords(submission.title)

    if len(keywords) == 0:
        return Response()

    return Response(
        reply=construct_advice(keywords),
        report=f"Excessive promo? Title: {join_keywords(keywords)}"
    )
