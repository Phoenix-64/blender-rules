import random

import config.blender_flairs as flairs
from bot.response import Response

CATCHPHRASES = [
    "So you have chosen death.",
    "Initiating roast sequence.",
    "Pfft a bot could roast that.",
    "I love the smell of a freshly-roasted render.",
    "The game is on.",
]


def generate_warning():
    catchphrase = random.choice(CATCHPHRASES)

    return f"""
{catchphrase}

Just a friendly heads-up to prevent misunderstandings – the "roast my render" flair is if you want to get harsh & destructive criticism :)

If you're not ready for this, there's also the "need feedback" flair to ask for nicer suggestions. Feel free to change the flair if this was a mistake. Otherwise, enjoy the roast!
"""


def get_roast_warning_response(submission):
    flair = submission.link_flair_template_id if hasattr(submission, "link_flair_template_id") else None

    if flair == flairs.ROAST_MY_RENDER:
        return Response(
            reply=generate_warning(),
            is_casual=True
        )

    return Response()
