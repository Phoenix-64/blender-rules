import re

from bot.response import Response

BUG_MESSAGE = """
Could that be a bug? Reddit is probably not the best place to report Blender bugs – the developers don't really check here.

Please see [this guide](https://wiki.blender.org/wiki/Process/Bug_Reports) for what to do if you think you've found a bug. Thanks!
"""

RESOURCE_MESSAGE = """
Are you asking about free Blender resources? Here's a [Blender StackExchange thread](https://blender.stackexchange.com/q/15355/49806) with a list of great resources!
"""

NEW_USER_MESSAGE = """
New to Blender? Welcome! I can suggest the [donut tutorial](https://www.youtube.com/watch?v=TPrnSACiTJ4) – that's where people usually start. It should give you a good understanding of the basics of Blender. After that, you can explore more specific tutorials for what you want!
"""

resource_type = "(resources?|websites?|library|libraries|assets?|textures?|materials?|models?|hdris?|tools?|add?-?ons?)"

FAQ_RESPONSES = [
    (
        re.compile("\\b(crash(ed|es|ing)|bug)\\b", flags=re.IGNORECASE),
        BUG_MESSAGE
    ),
    (
        re.compile(
            f"(\\b(free|good|suggest|recommend|find) (\\w+ ){{0,5}}){resource_type}\\b|"
            f"\\b{resource_type} (free|suggestions?|recomendations?|for blender)\\b",
            flags=re.IGNORECASE),
        RESOURCE_MESSAGE
    ),
    (
        re.compile(
            "\\b(don'?t know|dunno|wondering|no idea) (where|how) to (start|begin|get started)\\b|"
            "\\b(where|how) (do|can|should|would) (i|you|u) (\w+ )?(start|begin|get started)\\b",
            flags=re.IGNORECASE),
        NEW_USER_MESSAGE
    ),
]


def get_faq(text):
    for (pattern, message) in FAQ_RESPONSES:
        if bool(pattern.search(text)):
            return message
    return None


def get_faq_response(submission):
    full_text = f"{submission.title} {submission.selftext}"
    guide = get_faq(full_text)

    if guide is None:
        return Response()

    return Response(reply=guide)
