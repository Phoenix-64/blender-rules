from rules_post.caps_title import is_caps

expected_caps = {
    "MEET YER ZOGGIN KRUMPY TERMINATORK": True,

    "Fluid simulation. Aquaman scene from justice league": False,
    "Another Shot from my Monthly Project": False,
}


def test_is_caps():
    for title, expected_is_caps in expected_caps.items():
        assert is_caps(title) == expected_is_caps
