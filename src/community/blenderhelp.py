import praw

from bot.response import Response, get_first_actionable_response
from community.community import Community
from rules_comment.constructive_criticism import get_constructive_criticism_response
from rules_comment.good_bot import get_good_bot_response
from rules_comment.swearing import get_swearing_response
from rules_post.caps_title import get_caps_title_response
from rules_post.faq import get_faq_response


class BlenderHelp(Community):
    def get_submission_response(self, submission: praw.models.Submission) -> Response:
        serious_response = get_first_actionable_response([
            get_faq_response(submission)
        ])

        # easter eggs and less serious responses
        # (only if none of the "priority" helpful responses were triggered)
        response = get_first_actionable_response([
            serious_response,
            get_caps_title_response(submission)
        ])

        return response

    def get_comment_response(self, comment: praw.models.Comment) -> Response:
        swearing_response = get_swearing_response(comment)

        if not swearing_response.is_nothing():
            return swearing_response

        return get_first_actionable_response([
            get_constructive_criticism_response(comment),
            get_good_bot_response(comment),
        ])
