import abc

import praw

from bot.response import Response


class Community(abc.ABC):
    @abc.abstractmethod
    def get_submission_response(self, submission: praw.models.Submission) -> Response:
        pass

    @abc.abstractmethod
    def get_comment_response(self, submission: praw.models.Comment) -> Response:
        pass
