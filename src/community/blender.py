import praw

from bot.response import Response, merge_responses, get_first_actionable_response
from community.community import Community
from rules_comment.constructive_criticism import get_constructive_criticism_response
from rules_comment.good_bot import get_good_bot_response
from rules_comment.swearing import get_swearing_response
from rules_comment.outdated_unsolved_flair import get_outdated_unsolved_flair_response
from rules_post.caps_title import get_caps_title_response
from rules_post.excessive_promotion import get_excessive_promotion_response
from rules_post.missing_critique_flair import get_missing_critique_flair_response
from rules_post.missing_help_flair import get_missing_help_flair_response
from rules_post.roast_warning import get_roast_warning_response
from rules_post.robot_artwork import get_robot_artwork_response



class Blender(Community):
    def get_submission_response(self, submission: praw.models.Submission) -> Response:
        serious_response = merge_responses([
            get_roast_warning_response(submission),
            get_excessive_promotion_response(submission),
            get_first_actionable_response([
                get_missing_help_flair_response(submission),
                get_missing_critique_flair_response(submission),

            ])
        ])

        # easter eggs and less serious responses
        # (only if none of the "priority" helpful responses were triggered)
        response = get_first_actionable_response([
            serious_response,
            get_caps_title_response(submission),
            get_robot_artwork_response(submission),
        ])

        return response

    def get_comment_response(self, comment: praw.models.Comment) -> Response:
        swearing_response = get_swearing_response(comment)

        if not swearing_response.is_nothing():
            return swearing_response

        return get_first_actionable_response([
            get_constructive_criticism_response(comment),
            get_good_bot_response(comment),
            get_outdated_unsolved_flair_response(comment),
        ])
