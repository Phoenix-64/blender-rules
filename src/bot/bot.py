import praw

from bot.reddit_api_util import get_item_stream
from bot.response import make_response
from community.blender import Blender
from community.blenderhelp import BlenderHelp
from config.env import USERNAME

COMMUNITIES = {
    "blender": Blender(),
    "blenderhelp": BlenderHelp(),
}


def create_bot(client_id, client_secret, username, password):
    reddit = praw.Reddit(
        user_agent="blender-rules (bot by u/reinis-mazeiks)",
        client_id=client_id,
        client_secret=client_secret,
        username=username,
        password=password,
    )

    subreddits = [reddit.subreddit(name) for name in COMMUNITIES.keys()]

    for item in get_item_stream(subreddits):
        if isinstance(item, praw.models.Comment):
            process_comment(item)
        elif isinstance(item, praw.models.Submission):
            process_submission(item)
        else:
            raise Exception("Unexpected item in stream", item)


def process_comment(comment):
    if comment.author.name == USERNAME:
        return

    sub = comment.subreddit.display_name
    response = COMMUNITIES[sub].get_comment_response(comment)
    make_response(comment, response)


def process_submission(submission):
    sub = submission.subreddit.display_name
    response = COMMUNITIES[sub].get_submission_response(submission)
    make_response(submission, response)
