from typing import Optional, List, Union

import praw

from bot.reddit_api_util import reddit_url
from config.mode import is_dry_run, is_production
from language_util.bot_language import construct_report, format_post_advice


class Response:
    reply: Optional[str]
    report: Optional[str]
    is_casual: bool  # (omit bot signature)

    def __init__(self, *_, reply=None, report=None, is_casual=False):
        self.reply = reply
        self.report = report
        self.is_casual = is_casual

    def is_nothing(self):
        return self.reply is None and self.report is None


def merge_responses(responses: List[Response]):
    replies = [
        response.reply
        for response in responses
        if response.reply is not None
    ]
    reply = "\n\nAlso...\n\n".join(replies) if len(replies) > 0 else None

    reports = [
        response.report
        for response in responses
        if response.report is not None
    ]
    report = "|".join(reports) if len(reports) > 0 else None

    is_casual = all(
        response.is_casual
        for response in responses
        if not response.is_nothing()
    )

    return Response(reply=reply, report=report, is_casual=is_casual)


def get_first_actionable_response(responses: List[Response]):
    for response in responses:
        if not response.is_nothing():
            return response
    return Response()


def preview_comment(comment: praw.models.Comment):
    return comment.body


def preview_submission(submission: praw.models.Submission):
    return f"# {submission.title}\n{submission.selftext}"


def fake_response(item: Union[praw.models.Comment, praw.models.Submission], response):
    print()
    if isinstance(item, praw.models.Comment):
        print(preview_comment(item))
    elif isinstance(item, praw.models.Submission):
        print(preview_submission(item))
    else:
        raise Exception("Unexpected item type", item)

    if response.reply is not None:
        print(f"Reply: {response.reply}")

    if response.report is not None:
        print(f"Report: {response.report}")

    print("-" * 20)


def make_response(item: Union[praw.models.Comment, praw.models.Submission], response):
    if response.is_nothing():
        return

    print(reddit_url(item.permalink))

    if is_dry_run():
        return fake_response(item, response)
    elif is_production():
        if response.reply is not None:
            needs_signature = isinstance(item, praw.models.Submission) and not response.is_casual
            message = format_post_advice(response.reply) if needs_signature else response.reply
            item.reply(message)

        if response.report is not None:
            message = construct_report(response.report)
            item.report(message)
    else:
        raise Exception("Unexpected mode")
