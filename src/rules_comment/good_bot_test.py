from rules_comment.good_bot import is_good_bot_comment


def test_is_good_bot_comment():
    assert is_good_bot_comment("Good bot!")
    assert is_good_bot_comment("good bot")
    assert is_good_bot_comment("good bot.")
    assert is_good_bot_comment("good bot :)")
    assert is_good_bot_comment(":) good bot")

    assert not is_good_bot_comment("bad bot")
    assert not is_good_bot_comment("no")
    assert not is_good_bot_comment("not good bot")