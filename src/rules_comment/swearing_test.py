from rules_comment.swearing import find_obscenities

expected_obscenities = {
    "yippee whatever motherfucker": {"motherfucker"},
    "some whoreface broke into my house": {"whoreface"},
    "not ment for you easily upset biiiitch, fuck off and get a life": {"biiiitch", "fuck off"},
    "I think you just enjoy being a cunt.": {"cunt"},
    "There are also games like doom, metro exodus, battlefield, control, and tons of others, so why not shut the fuck up instead of coming up with unsourced bullshit based on the worst examples possible?": {
        "shut the fuck"},
    "That's the thing, I used those as an example because it's full of new comers. Even the leads are fairly inexperienced. Bethesda still has old school programmers leading so you, can't use that as an example. So how about you \"shut the fuck instead of coming up with unsourced bullshit \" lol": {
        "shut the fuck"},
    "What an ignorant motherfucker. It's made by Id software. Bethesda is the publisher. And what about the other examples? What a clown": {
        "motherfucker"},
}


def test_find_obscenities():
    for text, expected in expected_obscenities.items():
        assert find_obscenities(text) == expected
