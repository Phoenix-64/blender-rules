import re

from bot.response import Response
from language_util.bot_language import join_keywords

trash = "(shit|crap|garbage|trash|failure|fail)"
bad = "(bad|terrible|horrible|stupid)"

MEAN_KEYWORDS = re.compile(
    f"(^|(?<=([.?!,] )))(\\w+ ){{0,2}}(looks) (\\w+ ){{0,2}}({trash})\\b|"
    f"(^|(?<=([.?!,] )))(\\w+ ){{0,2}}(you|this|that|it|those|these)( is|'?s| are) (\\w+ ){{0,2}}({trash})\\b|"
    f"\\b(you)('?re| are) (\\w+ ){{0,4}}({bad}|{trash}|idiot|noob)\\b|"
    f"\\b(you)('?ve| have) (\\w+ )?no (\\w+ ){{0,2}}(talent|skills)\\b|"
    f"\\b(who (would )?upvote[sd]|nobody likes) (\\w+ ){{0,2}} (this|that) {trash}",
    flags=re.IGNORECASE
)


def find_mean_words(text):
    return {
        violation.group()
        for violation in MEAN_KEYWORDS.finditer(text)
    }


def get_constructive_criticism_response(comment):
    if comment.is_submitter:
        return Response()

    mean_words = find_mean_words(comment.body)

    if len(mean_words) == 0:
        return Response()

    return Response(
        report=f"Possibly rude: {join_keywords(mean_words)}"
    )
