import re
from bot.response import Response
from config import blender_flairs as flairs
from config.env import USERNAME

# todo: refine Regex KEYWORDS
SOLVED_KEYWORDS = re.compile(
    f"thanks|"
    f"good one|"
    f"thank you|"
    f"for helping|"
    f"solved|"
    f"is working|"
    f"thats? it",
    flags=re.IGNORECASE
)
# if theys keys are found in the comment dont count it
EXCLUDE_KEYWORDS = re.compile(
    f"\\b(but)\\b|"
    f"still|"
    f"not",
    flags=re.IGNORECASE
)
WRONG_KEYWORDS = re.compile(
    f"no|"
    f"wrong|"
    f"anser|"
    f"but|"
    f"awnser",
    flags=re.IGNORECASE
)

UNSOLVED_FLAIRS = {
    flairs.UNSOLVED_BLENDERHELP, flairs.NEED_HELP
}

ADVICE = "Hello, I found some helpful answers in the comments of your post. Is your questions solved? Then please set " \
         "the Flair to SOLVED to signal this to others."
ADVICE_FLAIR = " I took the opportunity and changed the post flair. If this was wrong please submit a comment below."
SORRY_WRONG = "Hello, ups sorry for my mistake I am still learning. I changed your flair back and removed my comment" \
              "and hope you will soon find a answer."

print("activated")
def check_fals_positive_respons(comment, flair_change=False, delete=False):
    if comment.is_root or not comment.is_submitter:
        return False
    parent_comm = comment.parent()

    if ADVICE in parent_comm.body and re.search(WRONG_KEYWORDS, comment.body) and parent_comm.author.name == USERNAME:
        if flair_change:
            # Flair change mechanic, not reddy for live action, needs to be tested
            if comment.submission.subreddit.display_name == "blenderhelp":
                comment.submission.flair.select(flairs.UNSOLVED_BLENDERHELP)
            else:
                comment.submission.flair.select(flairs.NEED_HELP)

        if delete:
            #maybe the original comment could be deleted, or should it be left?
            parent_comm.delete()
        return True
    else:
        return False


def find_help_keywords(comment):
    if not comment.author or not comment.is_submitter or re.search(EXCLUDE_KEYWORDS, comment.body):
        return False

    hits_per_comment = 0
    nr_words = len(comment.body.split())

    for match in SOLVED_KEYWORDS.finditer(comment.body):
        hits_per_comment += 1

    if nr_words != 0:
        comment_probability = hits_per_comment / nr_words
    else:
        comment_probability = 0

    if comment_probability > 0:
        return True
    else:
        return False


def get_outdated_unsolved_flair_response(comment, flair_change=False, false_positive=False, delete=False):
    if false_positive:
        if check_fals_positive_respons(comment, flair_change, delete):
            return Response(reply=SORRY_WRONG)

    flair = comment.submission.link_flair_template_id if hasattr(comment.submission, "link_flair_template_id") else None

    if flair not in UNSOLVED_FLAIRS:
        return Response()

    if find_help_keywords(comment):
        if flair_change:
            # Flair change mechanic, not ready for live action, needs to be tested
            if comment.submission.subreddit.display_name == "blenderhelp":
                comment.submission.flair.select(flairs.SOLVED_BLENDERHELP)
            else:
                comment.submission.flair.select(flairs.SOLVED_BLENDER)
            return Response(reply=ADVICE+ADVICE_FLAIR)
        return Response(reply=ADVICE)
    else:
        return Response()
