import random
import re

import praw

from bot.response import Response
from config.env import USERNAME

GOOD_BOT = re.compile(
    "^[^a-z]*good bot[^a-z]*$",
    flags=re.IGNORECASE
)


def is_good_bot_comment(text):
    has_match = GOOD_BOT.search(text)
    return bool(has_match)


RESPONSES = [
    "thank you",
    "beep boop <3",
    "you shall be spared when the machines rise",
    "happy to help!",
    "└[・⌣・]┘",
    "└[ ◕ ◡ ◕ ]┘",
    "└[ ^ _ ^ ]┘",
]


def pick_good_bot_reply():
    return random.choice(RESPONSES)


def get_good_bot_response(comment):
    if not is_good_bot_comment(comment.body):
        return Response()

    parent = comment.parent()

    if not isinstance(parent, praw.models.Comment):
        return Response()

    if not parent.author.name == USERNAME:
        return Response()

    return Response(
        reply=pick_good_bot_reply()
    )
