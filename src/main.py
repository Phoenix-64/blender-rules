from bot.bot import create_bot
from config.env import PASSWORD, USERNAME, CLIENT_SECRET, CLIENT_ID, MODE

print(f"Starting bot {USERNAME} ({MODE})")


create_bot(
    username=USERNAME,
    password=PASSWORD,
    client_id=CLIENT_ID,
    client_secret=CLIENT_SECRET
)
