from config.links import ABOUT_BOT, BOT_FEEDBACK


def join_keywords_parts(keywords):
    size = len(keywords)
    for i, keyword in enumerate(keywords):
        yield f"\"{keyword}\""

        if i < size - 1:
            yield ", " if i < size - 2 else " and "


def join_keywords(keywords, max=3):
    return "".join(join_keywords_parts(list(keywords)[:max]))


def format_post_advice(advice):
    return f"""
Hi, friendly bot here!

{advice}

---

*[About]({ABOUT_BOT}) | [Feedback]({BOT_FEEDBACK})*
"""


REPORT_LENGTH_LIMIT = 100


def construct_report(message):
    return f"[blender-rules-bot] {message}"[:100]
