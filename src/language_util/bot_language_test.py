from language_util.bot_language import join_keywords


def test_join_keywords():
    assert join_keywords(["hello"]) == "\"hello\""
    assert join_keywords(["hello", "world"]) == "\"hello\" and \"world\""
    assert join_keywords(["well", "hello", "world"]) == "\"well\", \"hello\" and \"world\""